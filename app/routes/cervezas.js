const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()


router.get('/', (req, res) => {
    console.log('rutas de cervezas')
    // let page = req.query.page ? req.query.page : 1;
    let page = req.query.page || 1;
  res.json({ mensaje: `Lista de cervezas, página ${page}` })
})

router.get('/:id', (req, res) => {
    console.log('rutas de show cervezas')
  res.json({ mensaje: `¡A beber ${req.params.id}!` })
})

router.post('/', (req, res) => {
  res.json({ mensaje: `Cerveza creada: ${req.body.nombre}` })
})

router.delete('/', (req, res) => {
  res.json({ mensaje: '¡Cerveza borrada!' })
})

router.put('/', (req, res) => {
  res.json({ mensaje: '¡Cerveza actualizada!' })
})

module.exports = router
